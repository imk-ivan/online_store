const {Device, DeviceInfo} = require('../models/models');

const isDeviceExist = async (id) => {
    const device = await Device.findOne({where:{id}})
    return device ? true : false;
}

const isDeviceInfoExist = async (deviceId, title) => {
    const device = await DeviceInfo.findOne({where:{deviceId, title}})
    return device ? true : false;
}

module.exports = {
    isDeviceExist,
    isDeviceInfoExist,
}