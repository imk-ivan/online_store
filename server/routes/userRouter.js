const Router = require('express');
const userController = require('../controllers/userController');
const authMiddleware = require('../middleware/authMiddleware');
const asyncHandler = require('express-async-handler')


const router = new Router();

router.post('/registration', asyncHandler(userController.registration))
router.post('/login', asyncHandler(userController.login))
router.get('/auth', authMiddleware(), asyncHandler(userController.check))

module.exports = router;