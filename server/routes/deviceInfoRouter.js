const Router = require('express');
const deviceInfoController = require('../controllers/deviceInfoController');
const authMiddleware = require('../middleware/authMiddleware');
const asyncHandler = require('express-async-handler')

const router = new Router();

router.post('/', authMiddleware('ADMIN'), asyncHandler(deviceInfoController.create));
router.delete('/delete/:infoId', authMiddleware('ADMIN'), asyncHandler(deviceInfoController.remove))

module.exports = router;