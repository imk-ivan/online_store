const Router = require('express');
const authMiddleware = require('../middleware/authMiddleware');
const deviceController = require('../controllers/deviceController')

const router = new Router();

router.post('/', authMiddleware('ADMIN'), deviceController.create)
router.get('/', deviceController.getAll)
router.get('/:id', deviceController.getById)

module.exports = router;