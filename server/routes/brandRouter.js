const Router = require('express');
const brandController = require('../controllers/brandController');
const authMiddleware = require('../middleware/authMiddleware');

const router = new Router();

router.get('/', brandController.getAll)
router.post('/', authMiddleware('ADMIN'), brandController.create)

module.exports = router;