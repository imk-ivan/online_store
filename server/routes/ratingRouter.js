const Router = require('express');
const ratingController = require('../controllers/ratingController');
const authMiddleware = require('../middleware/authMiddleware');
const asyncHandler = require('express-async-handler')

const router = new Router();

router.post('/', authMiddleware(), asyncHandler(ratingController.addRating));
router.delete('/:ratingId', authMiddleware(), asyncHandler(ratingController.removeRating));

module.exports = router;


