const Router = require('express');
const typeController = require('../controllers/typeController');
const authMiddleware = require('../middleware/authMiddleware');

const router = new Router();

router.get('/', typeController.getAll)
router.post('/', authMiddleware('ADMIN'), typeController.create)

module.exports = router;