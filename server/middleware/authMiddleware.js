const jsonwebtoken = require('jsonwebtoken');
const config = require('../config');

module.exports = function(role){
    return function(req, res, next){
        if(req.method === 'OPTIONS'){
            next()
        }
        try{
            const token = req.headers.authorization.split(' ')[1];
            if(!token){
                return res.status(403).json({message:"Forbidden. Invalid user token"})
            }
    
            const decoded = jsonwebtoken.verify(token, config.AUTH_SECRET_KEY);

            if(role && role !== decoded.role){
                return res.status(403).json({message: 'Forbidden. You have no permission to this action.'})
            }

            req.user = decoded;
            next();
    
        }
        catch(e){
            return res.status(403).json({message:"Forbidden. User not authorized"})
        }
    }
}