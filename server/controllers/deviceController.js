const uuid = require('uuid');
const path = require('path');
const ApiError = require('../error/ApiError');
const {Device, DeviceInfo} = require('../models/models');

class DeviceController {

    async create(req, res, next){

        try{

            const {
                name,
                price,
                typeId,
                brandId,
                info,
            } = req.body;

            const {img} = req.files;
            const fileName = uuid.v4() + '.jpg'
            img.mv(path.resolve(__dirname, '..', 'static', fileName));
    
            const device = await Device.create({name, price, typeId, brandId, img: fileName});

            if(info){
                const infoArr = JSON.Parse(info);
                infoArr.forEach(infoProp => {
                    DeviceInfo.create({
                        title: infoProp.title,
                        description: infoProp.description,
                        deviceId: device.id
                    })
                })
            }
    
            return res.json(device);
        }
        catch(ex){
            next(ApiError.internalError(ex.message))
        }
    }

    async getAll(req, res){
        const{brandId, typeId, limit = 10, page = 1} = req.query;
        let filterQuery = {}
        const offset = (page - 1) * limit;

        if(brandId){
            filterQuery.brandId = brandId;
        }
        if(typeId){
            filterQuery.typeId = typeId;
        }

        const devices = await Device.findAndCountAll({where: filterQuery, limit, offset});

        return res.json(devices);
    }

    async getById(req, res, next){
        try{
            const{id} = req.params;

            const device = await Device.findOne({
                where: {id},
                include: [{model: DeviceInfo, as: 'info'}]
            })
    
            return res.json(device);
        }
        catch(err){
            next(ApiError.internalError(err.message))
        }
    }

}

module.exports = new DeviceController();