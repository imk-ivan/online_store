const bcrypt = require('bcrypt');
const jsonwebtoken = require('jsonwebtoken');
const config = require('../config');
const ApiError = require('../error/ApiError');
const {User, Cart} = require('../models/models');

const isUserExists = async (email) => {
    const user = await User.findOne({where:{email}});
    return user ? true : false;
}

const generateJwt = (userId, email, role) =>{
    return jsonwebtoken.sign(
        {id:userId, email, role},
        config.AUTH_SECRET_KEY,
        {expiresIn:'24h'}
    );
}

class UserController {

    async registration(req, res, next){
        const {email, password, role = 'USER'} = req.body;

        if(!email || !password){
            return next(ApiError.badRequest("Data should contains email and password"))
        }

        const isUserAlreadyExist = await isUserExists(email);
        if(isUserAlreadyExist){
            return next(ApiError.badRequest("User with such email already exists"))
        }

        try{
            const passwordHash = await bcrypt.hash(password, 5);
            const user = await User.create({email, password: passwordHash, role})
            const cart = await Cart.create({userId: user.id})
    
            const token = generateJwt(user.id, email, role);
    
            return res.json({token});
        }
        catch(err){
            return next(ApiError.internalError("Cannot create new user - "+ err.message))
        }

    }

    async login(req, res, next){
        const {email, password} = req.body;

        const user = await User.findOne({where:{email}});
        if(!user){
            return next(ApiError.badRequest("Invalid user email"));
        }

        const comparePassword = await bcrypt.compare(password, user.password);
        if(!comparePassword){
            return next(ApiError.badRequest("Invalid password"));
        }

        const token = generateJwt(user.id, user.email, user.role);
        return res.json({token});
        
        // try{
           
        // }
        // catch(err){
        //     return next(ApiError.internalError("Cannot login. Please, try again later."))
        // }

    }

    async check(req, res, next){
        const token = generateJwt(req.user.id, req.user.email, req.user.role)
        return res.json({token})
    }

}

module.exports = new UserController();