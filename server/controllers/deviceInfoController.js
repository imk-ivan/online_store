const {isDeviceExist, isDeviceInfoExist} = require('../utils/utils');
const ApiError = require('../error/ApiError');
const {DeviceInfo, Device} = require('../models/models');

class DeviceInfoController {

    async create(req, res, next){
        const{
            deviceId,
            title,
            description
        } = req.body;

        const isDevExist = await isDeviceExist(deviceId);

        if(!isDevExist){
            return next(ApiError.badRequest("Device doesn't exist."));
        }

        const isPropertyExist = await isDeviceInfoExist(deviceId, title);
        if(isPropertyExist){
            return next(ApiError.badRequest("Property with such title already exists."));
        }

        try{
            const deviceInfo = await DeviceInfo.create({deviceId, title, description});
            return res.json({deviceInfo});
        }
        catch(ex){
            console.log(ex);
            return next(ApiError.internalError("Something went wrong. Please try again later."));
        }
        
    }

    async remove(req, res, next){
        const {infoId} = req.params;

        const info = await DeviceInfo.findOne({where:{id: infoId}});
        if(!info){
            return next(ApiError.badRequest("Property doesn't exist."));
        }

        try{
            const deleteResult = await DeviceInfo.destroy({where:{id:infoId}});

            return res.status(200).json({message:"Deleted succesfully"});
        }
        catch(ex){
            return next(ApiError.internalError("Something went wrong. Please try again later."))
        }
    }
}

module.exports = new DeviceInfoController();