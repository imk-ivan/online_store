const ApiError = require('../error/ApiError');
const {Rating, Device} = require('../models/models');

isRatingExist = async (userId, deviceId) => {
    const rating = await Rating.findOne({where:{userId, deviceId}});
    return rating ? true : false;
}

updateDeviceRating = async (deviceId) => {
    const deviceRatings = await Rating.findAndCountAll({where:{deviceId}});
    const averageRating = calculateDeviceRating(deviceRatings.rows, deviceRatings.count)
    const result = await Device.update(
        { rating: averageRating },
        { where: { id: deviceId } }
    )
    
    return result 
}

calculateDeviceRating = (ratings, count) => { 
    const initialValue = 0;
    const ratingsSumm = ratings.reduce(
        (prev, next) => prev + next.rate,
        initialValue
    );
    const averageRating = Math.round(ratingsSumm/count);
    return averageRating;
}

class RatingController{
    
    async addRating(req, res, next){
        const{user, body: {rate, deviceId}} = req;
        console.log('user-', user);
        

        if(!rate || !deviceId || !user){
            return next(ApiError.badRequest("Cant' rate device."));
        }

        const alreadyRated = await isRatingExist(user.id, deviceId);

        if(alreadyRated){
            return next(ApiError.badRequest("You have already rated this device."));
        }

        try{
            const rating = await Rating.create({rate, deviceId, userId: user.id});
            const UpdateRatingResult = await updateDeviceRating(deviceId);

            return res.json({rating, deviceRating: averageRating});
        }
        catch(err){
            return next(ApiError.badRequest("Cant' rate device: "+ err.message));
        }
    }

    async removeRating(req, res, next) {
        const{user} = req;
        const {ratingId} = req.params;

        if(!ratingId){
            return next(ApiError.badRequest("Invalid rating ID."));
        }

        const rating = await Rating.findOne({where:{id: ratingId}});

        if(!rating){
            return next(ApiError.badRequest("Rating doesn't exist."));
        }

        if(rating.userId != user.id && user.role !== 'ADMIN'){
            return next(ApiError.forbiddenError("You have no permission to this action."))
        }

        try{
            const deleteResult = await Rating.destroy({where:{id:ratingId}});
            const updateResult = await updateDeviceRating(rating.deviceId);

            return res.status(200).json({message:"Deleted succesfully"});
        }
        catch(ex){
            return next(ApiError.internalError("Something went wrong. Please try again later."))
        }
    }
}

module.exports = new RatingController();