export const ADMIN_ROUTE = '/admin';
export const LOGIN_ROUTE = '/login';
export const REGISTRATION_ROUTE = '/registration';
export const SHOP_ROUTE = '/';
export const CART_ROUTE = '/cart';
export const DEVICE_ROUTE = '/device';

// export{
//     ADMIN_ROUTE,
//     LOGIN_ROUTE,
//     REGISTRATION_ROUTE,
//     SHOP_ROUTE,
//     CART_ROUTE,
//     DEVICE_ROUTE
// }