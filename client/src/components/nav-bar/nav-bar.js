import React, {useContext} from 'react';
import {Navbar, Container, Nav } from 'react-bootstrap'
import { SHOP_ROUTE, CART_ROUTE, LOGIN_ROUTE } from '../../utils/constants';
import {observer} from 'mobx-react-lite'
import {UserContext} from './../../contexts';
import { Link } from 'react-router-dom';
import { ADMIN_ROUTE } from './../../utils/constants';

const NavBar = () => {
    const {userStore} = useContext(UserContext);

    const signOut = () => {
        userStore.setIsAuth(false);
        userStore.setUser({});
        localStorage.removeItem('token');
    }

    return (
        <div className='nav-bar'>
            <Navbar bg="dark" variant="dark">
                <Container>
                    <Navbar.Brand as={Link} to={SHOP_ROUTE}>SuperStore</Navbar.Brand>
                    {userStore.isAuth ?
                        <Nav className="ml-auto">
                            <Nav.Link as={Link} to={CART_ROUTE}>Cart</Nav.Link>
                            {userStore.user.role === 'ADMIN' && <Nav.Link as={Link} to={ADMIN_ROUTE}>Admin Panel</Nav.Link>}
                            <Nav.Link onClick={signOut}>Sign out</Nav.Link>
                        </Nav>
                        :
                        <Nav className="ml-auto">
                            <Nav.Link as={Link} to={LOGIN_ROUTE}>Sign In</Nav.Link>
                        </Nav>

                    }
                </Container>
            </Navbar>
        </div>
    )
}

export default observer(NavBar)
