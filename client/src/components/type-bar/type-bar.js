import React, {useContext} from 'react'
import { observer } from 'mobx-react-lite';
import UserContext from './../../contexts/user-context';
import { ListGroup, Spinner } from 'react-bootstrap';

const TypeBar = observer(() => {
    const {typeStore} = useContext(UserContext)
    if(typeStore.types.length < 1){
        return <Spinner animation={'grow'} />
    }
    return(
        <ListGroup>
            {typeStore.types.map(type => {
                return <ListGroup.Item
                            style={{cursor: 'pointer'}} 
                            key={type.id}
                            active={type.id === typeStore.selectedType}
                            onClick={() => typeStore.setSelectedType(type.id)}
                        >
                            {type.name}
                        </ListGroup.Item>
            })}
        </ListGroup>
    )
})

export default TypeBar
