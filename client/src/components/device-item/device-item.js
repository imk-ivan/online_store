import React from 'react'
import { observer } from 'mobx-react-lite';
import { Card, Col, Image } from 'react-bootstrap';
import { DEVICE_ROUTE } from '../../utils/constants';
import { NavLink } from 'react-router-dom';

function DeviceItem({device}) {
    return (
        <Col md={3}>
            <Card
                className="mt-4"
                style={{width: '150px'}}
                border={'light'}
            >   
                <NavLink to={`${DEVICE_ROUTE}/${device.id}`}>
                    <div className="dev-img-container" style={{width:'150px', height:'150px', textAlign: 'center'}}>
                        <Image style={{height:'150px', width: 'auto'}} src={process.env.REACT_APP_API_URL + device.img}/>
                    </div>
                    
                </NavLink>
                
                <div className="d-flex justify-content-between align-items-center mt-2 text-black-50">
                    <div>Samsung...</div>
                    <div>
                        <div>{device.rating} <i className="fa fa-star" aria-hidden="true" style={{fontSize:'13px'}}></i></div>  
                    </div>
                </div>
                <NavLink to={`${DEVICE_ROUTE}/${device.id}`}>
                    <div>{device.name}</div>
                </NavLink>
                
            </Card>
        </Col>
    )
}

export default observer(DeviceItem)
