import React, {useContext, useEffect, useState} from 'react'
import { BrowserRouter } from 'react-router-dom'
import AppRouter from '../app-router/app-router'
import NavBar from '../nav-bar'
import { observer } from 'mobx-react-lite';
import { UserContext } from '../../contexts';
import { check } from './../../http/userApi';
import { Spinner } from 'react-bootstrap';

function App() {
    const {userStore} = useContext(UserContext);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        check()
            .then(data => {
                console.log('data', data)
                userStore.setUser(data);
                userStore.setIsAuth(true);
            })
            .finally(() => setLoading(false));
    }, []);

    if(loading){
        return <Spinner animation={'grow'}/>
    }

    return (
        <BrowserRouter>
            <NavBar/>
            <AppRouter/>
        </BrowserRouter>
    )
}

export default observer(App)
