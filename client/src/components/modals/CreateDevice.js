import React, {useContext, useState} from 'react'
import { Form, Modal, Button, Dropdown, Row, Col } from 'react-bootstrap'
import UserContext from './../../contexts/user-context';

function CreateDevice({show, onHide}) {

    const {typeStore, brandStore} = useContext(UserContext)
    const [deviceInfo, setDeviceInfo] = useState([])

    const addnfo = () => {
        setDeviceInfo([...deviceInfo, {title: '', description: '', number: Date.now()}])
    }

    const removeInfo = (number) => {
        setDeviceInfo(deviceInfo.filter(info => info.number !== number))
    }

    return (
        <Modal
            show={show}
            onHide={onHide}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Add Device
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group className="mb-3">
                        <Dropdown>
                            <Dropdown.Toggle variant="secondary">Item type</Dropdown.Toggle>
                            <Dropdown.Menu>
                                {
                                    typeStore.types.map(type=>{
                                        return <Dropdown.Item key={type.id}>{type.name}</Dropdown.Item>
                                    })
                                }
                            </Dropdown.Menu>
                        </Dropdown>
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Dropdown>
                            <Dropdown.Toggle variant="secondary">Item brand</Dropdown.Toggle>
                            <Dropdown.Menu>
                                {
                                    brandStore.brands.map(brand=>{
                                        return <Dropdown.Item key={brand.id}>{brand.name}</Dropdown.Item>
                                    })
                                }
                            </Dropdown.Menu>
                        </Dropdown>
                    </Form.Group>

                    <Form.Group>
                        <Form.Control
                            className='mb-3' 
                            type={'text'}
                            placeholder={'New item title'}/>
                        <Form.Control
                            className='mb-3'  
                            type={'number'}
                            placeholder={'Price'}/>
                        <Form.Control
                            className='mb-3' 
                            type={'file'}/>
                    </Form.Group>
                   <Form.Group>
                       <Button 
                        variant={'outline-dark'}
                        onClick={addnfo}>
                            Add property
                        </Button>
                        {
                            deviceInfo.map(info=>{
                                return (
                                    <Row key={info.number} className="mt-2">
                                        <Col md={4}>
                                            <Form.Control
                                                placeholder={'Property title'}/>
                                        </Col>
                                        <Col md={4}>
                                            <Form.Control
                                                placeholder={'Property description'}/>
                                        </Col>
                                        <Col md={2}>
                                            <Button 
                                                variant={'outline-danger'}
                                                onClick={() => removeInfo(info.number)}>
                                                    Remove
                                                </Button>
                                        </Col>
                                    </Row>
                                )
                            })
                        }
                   </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant={'outline-dark'} onClick={onHide}>Close</Button>
                <Button variant={'outline-success'} onClick={onHide}>Add</Button>
            </Modal.Footer>
        </Modal>
    )
}

export default CreateDevice
