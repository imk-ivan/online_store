import React, {useContext} from 'react'
import { observer } from 'mobx-react-lite';
import UserContext from './../../contexts/user-context';
import { Row, Spinner } from 'react-bootstrap';
import DeviceItem from '../device-item/device-item';

function DeviceList() {

    const {deviceStore} = useContext(UserContext)

    if(deviceStore.devices.length < 1){
        return <Spinner animation={'grow'} />
    }

    return (
        <div className="device-list mt-4">
            <Row className="d-flex">
                {
                    deviceStore.devices.map(device => {
                        return <DeviceItem key={device.id} device={device}/>
                    })
                }
            </Row>
        </div>
    )
}

export default observer(DeviceList)
