import React, {useContext} from 'react'
import { observer } from 'mobx-react-lite';
import UserContext from './../../contexts/user-context';
import { Card, Col, Row, Spinner } from 'react-bootstrap';

function BrandBar() {

    const {brandStore} = useContext(UserContext)

    if(brandStore.brands.length < 1){
        return <Spinner animation={'grow'} />
    }

    return (
        <Row >
            <Col md={12} className="d-flex flex-wrap">
            {
                brandStore.brands.map(brand => {
                    return <Card
                                style={{cursor: 'pointer'}}
                                className="p-2" 
                                key={brand.id}
                                onClick={()=>brandStore.setSelectedBrand(brand.id)}
                                border={brandStore.selectedBrand === brand.id ? 'danger' : 'light'}>
                                    {brand.name}
                            </Card>
                })
            }
            </Col>

        </Row>
    )
}

export default observer(BrandBar)
