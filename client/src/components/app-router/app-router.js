import React, {useContext} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import { authRoutes, publicRoutes } from '../../routes';
import * as routeConsts from '../../utils/constants';
import {UserContext} from './../../contexts';
import { observer } from 'mobx-react-lite';

const AppRouter = () => {
    
    const {userStore} = useContext(UserContext);    
    return (
        <div>
            <Switch>
                {userStore.isAuth && authRoutes.map(({path, Component}) => {
                    return <Route key={path} path={path} component={Component} exact/>
                })}
                {publicRoutes.map(({path, Component}) => {
                    return <Route key={path} path={path} component={Component} exact/>
                })}
                <Redirect to={routeConsts.SHOP_ROUTE}/>
            </Switch>
        </div>
    )
}

export default observer(AppRouter)
