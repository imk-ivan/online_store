import DeviceStore from './device-store';
import TypeStore from './type-store';
import BrandStore from './brand-store';
import UserStore from './user-store';

export default class RootStore {

    constructor(){
        this.userStore = new UserStore(this);
        this.deviceStore = new DeviceStore(this);
        this.typeStore = new TypeStore(this)
        this.brandStore = new BrandStore(this);
    }

}