import { $host, $authHost } from "./index";

export const createBrand = async (brand) => {
    const response = await $authHost.post('api/brand', brand);
    return response.data;
}

export const fetchBrands = async () => {
    const response = await $host.get('api/brand');
    return response.data;
}