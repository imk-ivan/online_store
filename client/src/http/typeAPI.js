import { $host, $authHost } from "./index";

export const createType = async (type) => {
    const response = await $authHost.post('api/type', type);
    return response.data;
}

export const fetchTypes = async () => {
    const response = await $host.get('api/type');
    return response.data;
}