import { $host, $authHost } from "./index";

export const createDevice = async (type) => {
    const response = await $authHost.post('api/device', type);
    return response.data;
}

export const fetchDevices= async () => {
    const response = await $host.get('api/device');
    return response.data;
}

export const fetchOneDevice= async (id) => {
    const response = await $host.get(`api/device/${id}`);
    return response.data;
}