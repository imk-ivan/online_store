import ReactDOM from 'react-dom'
import React from 'react'
import App from './components/app';
import UserContext from './contexts/user-context';
import RootStore from './store/root-store';

const root = new RootStore();

ReactDOM.render(
    <UserContext.Provider value={{
            root: root,
            userStore: root.userStore,
            deviceStore: root.deviceStore,
            typeStore: root.typeStore,
            brandStore: root.brandStore,
    }}
    >
        <App/>
    </UserContext.Provider>,
    document.getElementById('root')
)