import React, {useState, useContext} from 'react'
import { Card, Container, Form, Button, Row } from 'react-bootstrap'
import { NavLink, useLocation, useHistory } from 'react-router-dom'
import { REGISTRATION_ROUTE, LOGIN_ROUTE, SHOP_ROUTE } from './../utils/constants';
import { registration, login } from './../http/userApi';
import { observer } from 'mobx-react-lite';
import UserContext from './../contexts/user-context';

function AuthPage() {

    const {userStore} = useContext(UserContext);

    const location = useLocation();
    const history = useHistory();
    const isLogin = location.pathname === LOGIN_ROUTE;
    
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    

    const signIn = async () => {
        try{
            const data = await login(email, password)
            console.log(data);
             userStore.setUser(data);
             userStore.setIsAuth(true);
             history.push(SHOP_ROUTE);  
        }
        catch(ex){
            console.log(ex);
            alert(ex.response.data.message)
        }    
    }

    const signUp = async () => {
        try{
            const data = await registration(email, password)
            console.log(data);
            userStore.setUser(data);
            userStore.setIsAuth(true)
            history.push(SHOP_ROUTE);
        }
        catch(ex){
            console.log(ex);
            alert(ex.response.data.message)
        }
    }

    const SignInForm = (
        <Card className="p-5" style={{width: '600px'}}>
            <h2 className="m-auto">Sign In</h2>
            <Form className="d-flex flex-column">
                <Form.Control 
                    className="mt-3" 
                    placeholder="Email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />

                <Form.Control 
                    type="password" 
                    className="mt-3" 
                    placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
                <Row className="d-flex justify-content-between mt-3">
                    <div style={{width: 'auto'}}>
                        Haven't account yet? <NavLink to={REGISTRATION_ROUTE}>Sign up</NavLink>
                    </div>
                    <div style={{width: 'auto'}}>
                        <Button variant={"outline-success"} onClick={signIn}>
                                Sign In
                        </Button>
                    </div>
                    
                </Row>

            </Form>
        </Card>
    )

    const SignUpForm = (
        <Card className="p-5" style={{width: '600px'}}>
            <h2 className="m-auto">Sign Up</h2>
            <Form className="d-flex flex-column">
                <Form.Control 
                    className="mt-3" 
                    placeholder="Email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />

                <Form.Control 
                    type="password" 
                    className="mt-3" 
                    placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                />
                <Row className="d-flex justify-content-between mt-3">
                    <div style={{width: 'auto'}}>
                        Already have an account? <NavLink to={LOGIN_ROUTE}>Sign up</NavLink>
                    </div>
                    <div style={{width: 'auto'}}>
                        <Button variant={"outline-success"} onClick={signUp}>
                                Sign Up
                        </Button>
                    </div>
                    
                </Row>

            </Form>
        </Card>
    )

    return (
        <Container 
            className="d-flex justify-content-center align-items-center"
            style={{height: window.innerHeight - 56}}
        >

            {isLogin ? SignInForm : SignUpForm}

        </Container>
    )
}

export default observer(AuthPage)
