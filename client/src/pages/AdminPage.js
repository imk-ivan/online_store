import React, { useState } from 'react'
import { Container, Button, Row, Col } from 'react-bootstrap'
import CreateBrand from '../components/modals/CreateBrand'
import CreateDevice from '../components/modals/CreateDevice'
import CreateType from '../components/modals/CreateType'

function AdminPage() {

    const [visibleBrandModal, setVisibleBrandModal] = useState(false);
    const [visibleTypeModal, setVisibleTypeModal] = useState(false);
    const [visibleDeviceModal, setVisibleDeviceModal] = useState(false);

    return (
        <Container className="mt-3">
            <Row>
                <Col md={4}>
                    <div className="d-flex flex-column">
                        <Button variant={'outline-dark'} className='mt-2' onClick={()=>setVisibleTypeModal(true)}>Add Type</Button>
                        <Button variant={'outline-dark'} className='mt-2' onClick={()=>setVisibleBrandModal(true)}>Add Brand</Button>
                        <Button variant={'outline-dark'} className='mt-2'onClick={()=>setVisibleDeviceModal(true)}>Add Device</Button>
                    </div>
                </Col>
            </Row>
            <CreateType show={visibleTypeModal} onHide={()=>setVisibleTypeModal(false)} />
            <CreateBrand show={visibleBrandModal} onHide={()=>setVisibleBrandModal(false)} />
            <CreateDevice show={visibleDeviceModal} onHide={()=>setVisibleDeviceModal(false)} />
        </Container>
    )
}

export default AdminPage
