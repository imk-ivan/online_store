import React, { useEffect, useState } from 'react'
import { Row, Col, Container, Image, Card, Button } from 'react-bootstrap'
import { useParams } from 'react-router';
import bigStar from '../assets/big-star.png';
import { fetchOneDevice } from './../http/deviceAPI';

function DevicePage() {

    const [device, setDevice] = useState({info:[]})

    const params = useParams()

    useEffect(() => {
        const deviceId = params.id;
        fetchOneDevice(deviceId)
            .then(data => setDevice(data))
    }, [])


    return (
        <div>
            <Container className="pt-5">
            <Row>
                <Col md={4}>
                    <div className='img-container' style={{width: '300px', height:'300px', textAlign: 'center'}}>
                        {device.img && <Image style={{width: 'auto', height:'300px'}} src={process.env.REACT_APP_API_URL + device.img}/>}
                    </div>
                    
                </Col>
                <Col md={4}>
                    <div className="d-flex flex-column justify-content-center align-items-center">
                        <h1>{device.name}</h1>
                        <div 
                            className="d-flex align-items-center justify-content-center"
                            style={{
                                background: `url(${bigStar}) no-repeat center center`,
                                width: 240,
                                height:240,
                                backgroundSize:'cover',
                                fontSize:64}}>
                            {device.rating}
                        </div>
                    </div>
                    
                </Col>
                <Col md={4}>
                    <Card 
                        className="d-flex flex-column align-items-center justify-content-around p-3"
                        style={{width:300, height:150}}
                    >
                        <h3>${device.price}</h3>
                        <Button variant="outline-dark">Add to cart</Button>
                    </Card>
                </Col>
            </Row>
            <Row className="mt-4">
                <Col md={5}>
                    <div className="mb-3">
                        <h3>Description</h3>
                    </div>
                    <ul className="list-group list-group-flush">
                        {device.info.map(info=>{
                            return <li key={info.id} className="list-group-item">
                                <div className="d-flex justify-content-between">
                                    <div>{info.title}</div>
                                    <div>{info.description}</div>
                                </div>
                            </li>
                        })}
                        {/* <li class="list-group-item">An item</li>
                        <li class="list-group-item">A second item</li>
                        <li class="list-group-item">A third item</li>
                        <li class="list-group-item">A fourth item</li>
                        <li class="list-group-item">And a fifth one</li> */}
                    </ul>
                </Col>
            </Row>
            </Container>
        </div>
    )
}

export default DevicePage
