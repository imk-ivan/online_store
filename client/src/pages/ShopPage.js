import React, {useContext, useEffect} from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import BrandBar from '../components/brand-bar/brand-bar'
import DeviceList from '../components/device-list/device-list'
import TypeBar from '../components/type-bar'
import { observer } from 'mobx-react-lite';
import { UserContext } from '../contexts'
import { fetchTypes } from '../http/typeAPI'
import { fetchBrands } from '../http/brandAPI'
import { fetchDevices } from '../http/deviceAPI'

function ShopPage() {

    const{
        deviceStore,
        typeStore,
        brandStore
    } = useContext(UserContext)

    useEffect(() => {
        fetchTypes()
            .then(data => {
                typeStore.setTypes(data);
            })

        fetchBrands()
            .then(data => {
                brandStore.setBrands(data);
            })

        fetchDevices()
            .then(data => {
                deviceStore.setDevices(data.rows);
            })
    }, [])

    return (
        <Container className="pt-5">
            <Row>
                <Col md={3}>
                    <TypeBar />
                </Col>
                <Col md={9}>
                    <BrandBar/>
                    <DeviceList/>
                </Col>
            </Row>
        </Container>
    )
}

export default observer(ShopPage)
