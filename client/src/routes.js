import AdminPage from "./pages/AdminPage";
import CartPage from './pages/CartPage';
import ShopPage from './pages/ShopPage';
import AuthPage from './pages/AuthPage';
import DevicePage from './pages/DevicePage';
import * as routeConsts from './utils/constants';

export const authRoutes = [
    {
        path: routeConsts.ADMIN_ROUTE,
        Component: AdminPage
    },
    {
        path: routeConsts.CART_ROUTE,
        Component: CartPage
    },
]

export const publicRoutes = [
    {
        path: routeConsts.SHOP_ROUTE,
        Component: ShopPage
    },
    {
        path: routeConsts.REGISTRATION_ROUTE,
        Component: AuthPage
    },
    {
        path: routeConsts.LOGIN_ROUTE,
        Component: AuthPage
    },
    {
        path: routeConsts.DEVICE_ROUTE +'/:id',
        Component: DevicePage
    },
]